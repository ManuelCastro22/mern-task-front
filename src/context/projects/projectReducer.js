// Contiene las funciones que van a interactuar con el state del context de proyectos, es decir el reducer modifica el state
// Importo los types
import { 
    FORM_PROJECT,
    GET_PROJECTS,
    ADD_PROJECT,
    ERROR_PROJECT,
    VALIDATE_FORM,
    GET_CURRENT_PROJECT,
    DELETE_PROJECT
    } from '../../types/index'



export default ( state, action ) => {
    switch(action.type)  {
        case FORM_PROJECT:
            return {
                ...state, // Toma una copía del state del proyecto
                hasForm: true // Cambia el valor de hasForm que se encuentra en el state
            }
        case GET_PROJECTS:
            return {
                ...state, // Copía del state
                projects: action.payload // lo que se pasó como parámetro
            }
        case ADD_PROJECT:
            return {
                ...state, // Copía del state
                hasForm: false,
                errorForm: false,
                projects: [
                    ...state.projects, // Copía del state
                    action.payload // Lo que se pasó como parámetro
                ]
            }
        case VALIDATE_FORM:
            return {
                ...state,
                errorForm: true
            }
        case GET_CURRENT_PROJECT:
            return {
                ...state,
                currentProject: state.projects.filter( project => project._id === action.payload ) // Devuelve un array con el/los proyectos que coincidan con el id pasado como parámetro (payload)
            }
        case DELETE_PROJECT:
            return {
                ...state,
                projects: state.projects.filter( project => project._id !== action.payload ), // Devuelve un array con el/los proyectos que NO coincidan con el id pasado como parámetro (payload), es decir omitirá el objeto que tenga ese id (para simular la eliminación)
                currentProject: null // Resetea el proyecto actual (para que no aparezca la data de este visible)
            }
        case ERROR_PROJECT:
            return {
                ...state,
                message: action.payload
            }
        default: 
            return state
    }
}