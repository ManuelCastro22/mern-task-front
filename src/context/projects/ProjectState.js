// Contiene el state del context proyectos

import React, { useReducer } from 'react'
// importo el context y el reducer de proyectos
import projectContext from './projectContext'
import projectReducer from './projectReducer'
// Importo uuid
// import { v4 as uuidv4 } from 'uuid'
// // importo los types
import { 
    FORM_PROJECT,
    GET_PROJECTS,
    ADD_PROJECT,
    ERROR_PROJECT,
    VALIDATE_FORM,
    GET_CURRENT_PROJECT,
    DELETE_PROJECT
    } from '../../types/index'
// importo cliente axios
import clientAxios from '../../config/axios'



const ProjectState = props => { // Es como si fuera el provider visto en secciones anteriores

    // State initial
    const initialState = {
        projects: [], // Inicialmente no hay proyectos
        hasForm: false, // existe formulario de creación nuevo proyecto
        errorForm: false, // error en el formulario de nuevo proyecto
        currentProject: null, // proyecto actual
        message: null // Mensaje de errores
    }




    // Dispatch para ejecutar las acciones
    // El Hook, useReducer se usa , devuelve el state y la función de dispatch
    const [ state, dispatch ] = useReducer(projectReducer, initialState)



    // Mostrar el formulario para crear un nuevo proyecto
    const showForm = () => {
        dispatch({
            type: FORM_PROJECT // Cuando se llame esta función se ejecutará el type
        })
    }
    // Obtener proyectos
    const getProjects = async () => {

        try {

            const response = await clientAxios.get('/api/projects')

            // Insertar los proyectos al state
            dispatch({
                type: GET_PROJECTS,
                payload: response.data.projects
            })
            
        } catch(error) {
            
            const alert = {
                msg: 'Hubo un error',
                category: 'alerta-error'
            }
            
            // console.log(error)
            dispatch({
                type: ERROR_PROJECT,
                payload: alert
            })

        }

    }
    // Agregar proyecto
    const addProject = async project => {
        // project.id = uuidv4()

        try {

            const response = await clientAxios.post('/api/projects', project) // Envío la información del proyecto a crear

            // Insertar el proyecto en el state
            dispatch({
                type: ADD_PROJECT, // Cuando se llame a esta función se ejecutará el type
                payload: response.data
            })
            
        } catch(error) {
            
            const alert = {
                msg: 'Hubo un error',
                category: 'alerta-error'
            }
            
            // console.log(error)
            dispatch({
                type: ERROR_PROJECT,
                payload: alert
            })

        }
        
    }
    // Validar error
    const showError = () => {
        dispatch({
            type: VALIDATE_FORM  // Cuando se llame a esta función se ejecutará el type
        })
    }
    // Obtener proyecto actual
    const getCurrentProject = projectId => {
        dispatch({
            type: GET_CURRENT_PROJECT,
            payload: projectId
        })
    }
    // Eliminar proyecto
    const deleteProject = async projectId => {

        try {

            const response = await clientAxios.delete(`/api/projects/${projectId}`) // Petición para eliminar un proyecto según su id

            // Eliminar un proyecto por id
            dispatch({
                type: DELETE_PROJECT,
                payload: projectId
            })

        } catch(error) {

            const alert = {
                msg: 'Hubo un error',
                category: 'alerta-error'
            }
            
            // console.log(error)
            dispatch({
                type: ERROR_PROJECT,
                payload: alert
            })

        }

    }


    
    // Serie de funciones para el CRUD



    // Componente con provider
    return (
        <projectContext.Provider
            value={{ // Datos que estarán disponibles para los demás componentes
                projects: state.projects,
                hasForm: state.hasForm,
                errorForm: state.errorForm,
                message: state.message,
                currentProject: state.currentProject,
                showForm,
                getProjects,
                addProject,
                showError,
                getCurrentProject,
                deleteProject
            }}>
            { props.children }
        </projectContext.Provider>
    )

}

export default ProjectState