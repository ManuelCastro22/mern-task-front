// Se encargará de crear el contect

import { createContext } from 'react' // permite usar la función para crear un context

const taskContext = createContext() // Creo el context

export default taskContext // Exporto el context creado