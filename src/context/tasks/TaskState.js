// Contiene el state del context tareas

import React, { useReducer } from 'react'
// Importo el context y el reducer de tareas
import taskContext from './taskContext' // Importa para llamar el context creado
import taskReducer from './taskReducer' // Usará el reducer para cambiar el state
// Importo los types
import {
        TASKS_PROJECTS,
        DELETE_TASKS,
        ADD_TASK,
        VALIDATE_TASK,
        DELETE_TASK,
        STATE_TASK,
        UPDATE_CURRENT_TASK,
        UPDATE_TASK_DATA
    } from '../../types/index'
// Importo cliente axios
import clientAxios from '../../config/axios'
import Axios from 'axios'



const TaskState = props => { // Es como si fuera el provider visto en secciones anteriores
     
    // State inicial del context
    const initialState = {
        // tasks: [
        //     { id: 1, name: 'Tarea Dante', taskState: true, idProject: 1 },
        //     // { id: 2, name: 'Tarea Valencia', taskState: false, idProject: 3 },
        //     { id: 3, name: 'Tarea Mishi', taskState: false, idProject: 2 },
        //     // { id: 4, name: 'Tarea Scotty', taskState: false, idProject: 3 },
        //     { id: 5, name: 'Tarea Clarence', taskState: false, idProject: 3 },
        //     { id: 6, name: 'Tarea Douglas', taskState: true, idProject: 4 },
        //     { id: 7, name: 'Tarea Edy', taskState: false, idProject: 2 },
        //     { id: 8, name: 'Tarea Sea', taskState: false, idProject: 4 },
        //     { id: 9, name: 'Tarea Tom', taskState: false, idProject: 2 },
        //     { id: 10, name: 'Tarea WWw', taskState: false, idProject: 1 },
        //     { id: 11, name: 'Tarea Carl', taskState: true, idProject: 1 },
        //     { id: 12, name: 'Tarea Pinky', taskState: true, idProject: 4 },
        //     { id: 13, name: 'Tarea Double AA', taskState: true, idProject: 4 },
        //     { id: 14, name: 'Tarea Sagan', taskState: false, idProject: 2 }
        // ],
        tasksProject: [],
        errorTask: false,
        currentTask: null
    }



    // Dispatch para ejecutar las acciones
    // El Hook useReducer, devuelve el state y la función de dispatch
    const [ state, dispatch ] = useReducer(taskReducer, initialState) // Se pasá como parámetro el reducer y el state inicial y extraemos el state y los dispatch que son las acciones que modifican dicho state



    // FUNCIONES
    // Obtener las tareas de un proyecto según su id
    const getTasksByProject = async project => {

        try {

            const response = await clientAxios.get('/api/tasks', { params: { project }})

            console.log(response.data.tasks)

            dispatch({
                type: TASKS_PROJECTS, // Cuando se llame esta función se ejecutará el type
                payload: response.data.tasks
            })

        } catch(error) {

            console.log(error)

        }

    }

    // Elimina las tareas de un proyecto según su id
    const deleteTasksByProject = projectId => {
        dispatch({
            type: DELETE_TASKS, // Cuando se llame esta función se ejecutará el type
            payload: projectId
        })
    }

    // Agregar una nueva tarea
    const addTask = async newTask => {

        try {

            const response = await clientAxios.post('/api/tasks', newTask)

            dispatch({
                type: ADD_TASK, // Cuando se llame esta función se ejecutará el type
                payload: newTask
            })

        } catch(error) {
            console.log(error)
        }

    }

    // Validate tarea
    const validateTask = () => {
        dispatch({
            type: VALIDATE_TASK // Cuando se llame esta función se ejecutará el type
        })
    }

    // Eliminar tarea
    const deleteTask = async (taskId, project) => {

        try {

            const response = await clientAxios.delete(`/api/tasks/${taskId}`, {params: { project }})

            dispatch({
                type: DELETE_TASK, // Cuando se llame esta función se ejecutará el type
                payload: taskId
            })

        } catch(error) {
            console.log(error)
        }

    }

    // Cambia el state (en este caso puntual nos referimos a si está completado o no) del tares
    // const changeStateTask = task => {
    //     dispatch({
    //         type: STATE_TASK, // Cuando se llame esta función se ejecutará el type
    //         payload: task
    //     })
    // }

    // Actualizar la tarea
    const updateTaskData = async task => {

        try {
            
            const response = await clientAxios.put(`/api/tasks/${task._id}`, task)

            dispatch({
                type: UPDATE_TASK_DATA,
                payload: response.data.task
            })

        } catch(error) {
            console.log(error)
        }

    }

    // Actualizar la tarea actual (se refiere a actualizar la referencia a la tarea actual)
    const updateCurrentTask = task => {
        dispatch({
            type: UPDATE_CURRENT_TASK, // Cuando se llame esta función se ejecutará el type
            payload: task
        })
    }



    // Componente con provider (da acceso al state a los componentes)
    return (
        <taskContext.Provider
            value={{ // Datos que estarán disponibles para los demás componentes
                // tasks: state.tasks,
                tasksProject: state.tasksProject,
                errorTask: state.errorTask,
                currentTask: state.currentTask,
                getTasksByProject,
                deleteTasksByProject,
                addTask,
                validateTask,
                deleteTask,
                // changeStateTask,
                updateCurrentTask,
                updateTaskData
            }}>
            { props.children }
        </taskContext.Provider>
    )



}



export default TaskState