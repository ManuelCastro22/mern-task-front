// Contiene las funciones que van a interactuar con el state del context de tareas, es decir el reducer modifica el state

// Importo los types
// import Task from '../../components/tasks/Task'
import {
    TASKS_PROJECTS,
    DELETE_TASKS,
    ADD_TASK,
    VALIDATE_TASK,
    DELETE_TASK,
    STATE_TASK,
    UPDATE_CURRENT_TASK,
    UPDATE_TASK_DATA
} from '../../types/index'



// Exporto el reducer
export default ( state, action ) => { // Toma el state del context que modificará y el action que es la acción que modificará dicho state
    switch( action.type ){
        case TASKS_PROJECTS:
            return {
                ...state, // Copía del state
                tasksProject: action.payload // Filtra las tareas asociadas al id del proyecto ingresado en el payload
            }
        case DELETE_TASKS:
            return {
                ...state, // Copía del state
                tasksProject: state.tasksProject.filter( task => task.idProject !== action.payload ) // Elimina las tareas asociadas al id del proyecto ingresado en el payload
            }
        case ADD_TASK:
            return {
                ...state, // Copía del state
                tasksProject: [
                    action.payload, // Primero agrego el valor y luego genero la copía
                    ...state.tasksProject
                ],
                errorTask: false
            }
        case VALIDATE_TASK:
            return {
                ...state, // Copía del state
                errorTask: true
            }
        case DELETE_TASK:
            return {
                ...state, // Copía del state
                tasksProject: state.tasksProject.filter( task => task._id !== action.payload ) // Elimina la tarea que tenga el id pasado en el payload
            }
        case STATE_TASK: 
        case UPDATE_TASK_DATA: // Como ambos case hacen lo mismo se pueden dejar de esta forma
            return {
                ...state, // Copía del state
                tasksProject: state.tasksProject.map( task => { 
                    if( task._id === action.payload._id ){
                        return action.payload
                    } else {
                        return task
                    }
                }),
                currentTask: null
            }
        case UPDATE_CURRENT_TASK:
            return {
                ...state, // Copía del state
                currentTask: action.payload
            }
        default:
            return state
    }
}