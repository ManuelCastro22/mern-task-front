// Contiene el state del context tareas

import React, { useReducer } from 'react'
// Importo el context y el reducer de alerta
import alertContext from './alertContext' // Importa para llamar el context creado
import alertReducer from './alertReducer' // Usará el reducer para cambiar el state
// Importo los types
import {
    SHOW_ALERT,
    HIDE_ALERT
    } from '../../types/index'



const AlertState = props => { // Es como si fuera el provider visto en secciones anteriores

    // State inicial del context
    const initialState = {
        alert: null
    }



    // Dispatch para ejecutar las acciones
    // El Hook, useReducer se usa , devuelve el state y la función de dispatch
    const [ state, dispatch ] = useReducer( alertReducer, initialState )



    // Mostrar la alerta
    const showAlert = ( msg, category ) => {
        dispatch({
            type: SHOW_ALERT,
            payload: {
                msg,
                category
            }
        })

        // Después de 5 segundos oculta la alerta
        setTimeout( () => {
            dispatch({
                type: HIDE_ALERT
            })
        }, 5000 )
    }



    // Componente con provider
    return (
        <alertContext.Provider
            value={{ // Datos que estarán disponibles para los demás componentes
                alert: state.alert,
                showAlert
            }}>
                { props.children }
        </alertContext.Provider>
    )


}

export default AlertState