// Se encargará de crear el context

// permite usar la función para crear un context
import { createContext } from 'react'

// Creo el context
const alertContext = createContext()

// Exporto el context creado
export default alertContext