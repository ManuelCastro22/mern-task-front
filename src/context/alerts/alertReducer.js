// Contiene las funciones que van a interactuar con el state del context de tareas, es decir el reducer modifica el state

// Importo los types
import {
    SHOW_ALERT,
    HIDE_ALERT
    } from '../../types/index'



export default ( state, action ) => {
    switch(action.type) {
        case SHOW_ALERT:
            return {
                alert: action.payload
            }
        case HIDE_ALERT:
            return {
                alert: null
            }
        default:
            return state
    }
}