// Contiene el state del context tareas

import React, { useReducer } from 'react'
// Importo el context y el reducer de auth
import authContext from './authContext' // Importa para llamar el context creado
import authReducer from './authReducer' // Usará el reducer para cambiar el state
// importo el cliente axios creado con la variable de entorno
import axiosClient from '../../config/axios'
// Importo authToken para añadir al header del request el header
import authToken from '../../config/token'
// Importo los types
import {
    SUCCESS_REGISTER,
    ERROR_REGISTER,
    GET_USER,
    SUCCESS_LOGIN,
    ERROR_LOGIN,
    LOGOUT
    } from '../../types/index'
// Importo cliente axios
import clientAxios from '../../config/axios'



const AuthState = props => { // Es como si fuera el provider visto en secciones anteriores 

    // State inicial del context
    const initialState = {
        token: localStorage.getItem('token'), // Será el JWT (JSON Web Token) de autenticación que se almacenará en local storage
        authenticated: null, // Si está autenticada
        user: null, // Info del user
        message: null,
        loading: true // Sirve para solucionar el problema que deja ver otros componentes
    }



    // Dispatch para ejecutar las acciones
    // El Hook, useReducer se usa , devuelve el state y la función de dispatch
    const [ state, dispatch ] = useReducer( authReducer, initialState )



    // Registrar un usuario
    const registerUser = async info => {
        try {

            // Respuesta de la consulta
            const response = await axiosClient.post('/api/users', info) // Hacemos una consulta de tipo post mediante axios (usando la función que creamos) a la api de usuarios con los datos pasados en info

            // console.log(response)

            dispatch({
                type: SUCCESS_REGISTER,
                payload: response.data // Pasará como payload el token único que llega luego de crear un usuario en la DB
            })

            // Si el registro es exitoso, obtendrá los datos del usuario
            userAuthenticated()

        } catch(error) {
            // console.log(error.response.data.msg) // Así axios devuelve el mensaje de error

            // En este objeto almacena la respuesta del error para luego pasarlo como alerta al usuario
            const alert = {
                msg: error.response.data.msg, // mensaje
                category: 'alerta-error' // clase css del error
            }

            // En caso de que exista un error entonces se muestra el error
            dispatch({
                type: ERROR_REGISTER,
                payload: alert // pasa la respuesta de error como payload
            })

        }
    }

    // Retorna el usuario autenticado
    const userAuthenticated = async () => {

        // Obtiene el token almacenado en localstorage
        const token = localStorage.getItem('token')

        if(token) {

            // Función para enviar el token en el header de la petición
            authToken(token)

        }

        try {

            const response = await clientAxios.get('/api/auth')
            
            dispatch({
                type: GET_USER,
                payload: response.data.user // data del usuario obtenida del request
            })

        } catch(error) {

            // Si no hay token
            dispatch({
                type: ERROR_LOGIN
            })

        }

    }

    // Iniciar sesión
    const login = async data => {

        try {

            const response = await clientAxios.post('/api/auth', data) // Hacemos una consulta de tipo post mediante axios (usando la función que creamos) a la api de auth con los datos pasados en data

            dispatch({
                type: SUCCESS_REGISTER,
                payload: response.data // Pasará como payload el token único que llega luego de inciar sesión
            })

            // Si el registro es exitoso, obtendrá los datos del usuario
            userAuthenticated()

        } catch(error) {

            // En este objeto almacena la respuesta del error para luego pasarlo como alerta al usuario
            const alert = {
                msg: error.response.data.msg, // mensaje
                category: 'alerta-error' // clase css del error
            }

            // En caso de que exista un error entonces se muestra el error
            dispatch({
                type: ERROR_LOGIN,
                payload: alert // pasa la respuesta de error como payload
            })

        }

    }

    // Cerrar sesión
    const logout = () => {
        dispatch({
            type: LOGOUT
        })
    }



    // Componente con provider
    return (
        <authContext.Provider 
            value={{ // Datos que estarán disponibles para los demás componentes
                token: state.token,
                authenticated: state.authenticated,
                user: state.user,
                message: state.message,
                loading: state.loading,
                registerUser,
                userAuthenticated,
                login,
                logout
            }}>
            {props.children}
        </authContext.Provider>
    )

}

export default AuthState