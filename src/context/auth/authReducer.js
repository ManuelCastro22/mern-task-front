// Contiene las funciones que van a interactuar con el state del context de tareas, es decir el reducer modifica el state

// Importo los types
import {
    SUCCESS_REGISTER,
    ERROR_REGISTER,
    GET_USER,
    SUCCESS_LOGIN,
    ERROR_LOGIN,
    LOGOUT
    } from '../../types/index'


export default ( state, action ) => {
    switch(action.type) {
        case SUCCESS_LOGIN: // Ambos case usan el mismo código
        case SUCCESS_REGISTER:
            localStorage.setItem('token', action.payload.token) // Crea en el local storage un item con el token de JWT
            return {
                ...state, // Copia del state
                authenticated: true,
                message: null,
                loading: false
            }
        case LOGOUT: // Ambos case usan el mismo código
        case ERROR_LOGIN: // Ambos case usan el mismo código
        case ERROR_REGISTER:
            localStorage.removeItem('token') // Elimina algún token si existe, para evitar almacenar en local storage un token ajeno
            return {
                ...state, // Copía del state
                authenticated: false,
                token: null,
                user: null,
                message: action.payload,
                loading: false
            }
        case GET_USER:
            return {
                ...state, // Copía del state
                user: action.payload, // El usuario será lo que le pasemos en el payload, que será lo que se obtenga del request
                authenticated: true,
                loading: false
            }
        default:
            return state
    }
}