// Aquí se harán las consultas

// Importo axios
import axios from 'axios'



// Creo cliente axios
const clientAxios = axios.create({
    baseURL: process.env.REACT_APP_BACKEND_URL
})

// Exporto el cliente axios
export default clientAxios