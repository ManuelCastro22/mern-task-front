// Función para configurar el token que se envia en el header de cada petición

// Importo client axios
import clientAxios from './axios'

const authToken = token => {

    if( token ) {
        // Si hay un token lo agregra a la petición de axios
        clientAxios.defaults.headers.common['x-auth-token'] = token
    } else {
        // De lo contrario lo borra de la petición
        delete clientAxios.defaults.headers.common['x-auth-token']
    }

}

export default authToken