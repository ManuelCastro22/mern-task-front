// Acciones que describen que sucede en la aplicación

// PROYECTOS
export const FORM_PROJECT = 'FORM_PROJECT'
export const GET_PROJECTS = 'GET_PROJECTS'
export const ADD_PROJECT = 'ADD_PROJECT'
export const VALIDATE_FORM = 'VALIDATE_FORM'
export const GET_CURRENT_PROJECT = 'GET_CURRENT_PROJECT'
export const DELETE_PROJECT = 'DELETE_PROJECT'
export const ERROR_PROJECT = 'ERROR_PROJECT'

// TAREAS
export const TASKS_PROJECTS = 'TASKS_PROJECTS' // Obtener las tareas según el proyecto
export const DELETE_TASKS = 'DELETE_TASKS' // Borrar tareas asociadas a un proyecto
export const ADD_TASK = 'ADD_TASK' // Agregar una tarea
export const VALIDATE_TASK = 'VALIDATE_TASK' // Valida el campo de nueva tarea
export const DELETE_TASK = 'DELETE_TASK' // Borra una tarea
export const STATE_TASK = 'STATE_TASK' // Estado de la tarea (completado o no)
export const UPDATE_CURRENT_TASK = 'UPDATE_CURRENT_TASK' // Obtener/Actualizar la tarea actual (la que se usará para editar)
export const UPDATE_TASK_DATA = 'UPDATE_TASK_DATA' // Actualizar la tarea

// ALERTAS
export const SHOW_ALERT = 'SHOW_ALERT' // Mostrar alerta
export const HIDE_ALERT = 'HIDE_ALERT' // Ocultar alerta

// AUTENTICACIÓN
export const SUCCESS_REGISTER = 'SUCCESS_REGISTER' // Registro exitoso
export const ERROR_REGISTER = 'ERROR_REGISTER' // Registro con error
export const GET_USER = 'GET_USER' // Obtener usuario
export const SUCCESS_LOGIN = 'SUCCESS_LOGIN' // Inicio de sesión exitoso
export const ERROR_LOGIN = 'ERROR_LOGIN' // Inicio de sesión con error
export const LOGOUT = 'LOGOUT' // Cerrar sesión