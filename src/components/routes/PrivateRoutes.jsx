// Para proteger los componentes creamos un higher order component que es básciamente un componente que aloja otro componente adentro de el

import React, { useContext, useEffect } from 'react' // Importo hooks
// Importo React Router, lo instalamos con 'npm i react-router-dom'
import { Route, Redirect } from 'react-router-dom'
// Importamos la autenticación, necesaria para dar o no permisos a un usuario para acceder a determinados lugares
import authContext from '../../context/auth/authContext'



const PrivateRoutes = ({ component: Component, ...props }) => { // Le decimos que el componente pasado como parámetro será el componente que protegerá, al igual que una copía de las props de este

    // CONTEXT
    // Creando una variable con el contenido del context (lo extraemos con useContext)
    const authsContext = useContext(authContext)



    // Obteniedo los datos a usar del context de autenticación
    const { authenticated, loading, userAuthenticated } = authsContext



    // EFFECT
    useEffect( () => {
        // Mantiene la sesión iniciada para el usuario que este en el token del local storage
        userAuthenticated()
    }, [ ] )



    return (
        <Route 
            { ...props } // Copía de los props del componente
            render={
                props => !authenticated && !loading // Si el usuario no está autenticado y no está cargando
                    ?
                        ( <Redirect to="/" /> ) // redirigimos al home
                    :
                        ( <Component {...props} /> ) // redirigimos al componente con una copía de sus props
            }
        />
    )
    
}
 
export default PrivateRoutes