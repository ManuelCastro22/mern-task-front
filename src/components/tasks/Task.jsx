import React, { useState, useContext } from 'react'
// Importo el context y el reducer de tareas
import taskContext from '../../context/tasks/taskContext' // Importa para llamar el context creado
import projectContext from '../../context/projects/projectContext' // Importa para llamar el context creado



const Task = ( {task} ) => {

    // STATES



    // CONTEXT
    // Creando una variable con el contenido del context (lo extraemos con useContext)
    const tasksContext = useContext(taskContext)
    const projectsContext = useContext(projectContext)



    // Creando una variable con el contenido del context (lo extraemos con useContext)
    const { deleteTask, getTasksByProject, updateTaskData, updateCurrentTask} = tasksContext
    const { currentProject } = projectsContext



    // DESTRUCTURING
    const { _id, name, state } = task



    // FUNCIONES
    // Eliminar tarea por id
    const deleteTaskById = taskId => {
        deleteTask(taskId, currentProject[0]._id)
        getTasksByProject(currentProject[0]._id)
    }
    
    // Cambiar el estado (completado o no) de la tarea
    const changeStTask = task => {

        task.state = 
            task.state === true
            ? false
            : true

        updateTaskData(task)

    }

    // Actualizar tarea actual (cuando el usuario la quiere editar)
    const changeCurrentTask = taskItem => {
        updateCurrentTask(taskItem)
    }


    
    return (
        <li className="tarea">
            <p> { name } </p>
            <div className="estado">
                { task.state 
                    ? 
                        <button
                            type="button" 
                            className="completo" 
                            onClick={ () => changeStTask(task) } >
                            Completo
                        </button>
                    :
                        <button
                            type="button"
                            className="incompleto"
                            onClick={ () => changeStTask(task) } >
                            Incompleto
                        </button>
                }
            </div>
            <div className="acciones">
                <button
                    type="button"
                    className="btn btn-primario"
                    onClick={ () => changeCurrentTask(task) }>
                    Editar
                </button>
                <button
                    type="button"
                    className="btn btn-secundario" 
                    onClick={ () => deleteTaskById(_id) } >
                    Eliminar
                </button>
            </div>
        </li>
    )
}
 
export default Task