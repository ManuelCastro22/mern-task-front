import React, { useState, useContext, useEffect } from 'react'
// Importo los context de proyectos y tareas
import projectContext from '../../context/projects/projectContext'
import taskContext from '../../context/tasks/taskContext'
// Importo uuid
import { v4 as uuidv4 } from 'uuid'


const FormTask = () => {

    // STATES
    // Tarea
    const [ task, updateTask ] = useState({
        name: ''
    })



    // CONTEXT
    // Creando una variable con el contenido del context (lo extraemos con useContext)
    const projectsContext = useContext(projectContext)
    const tasksContext = useContext(taskContext)



    // Creando una variable con el contenido del context (lo extraemos con useContext)
    const { currentProject } = projectsContext
    const { currentTask, errorTask, getTasksByProject, addTask, validateTask, updateTaskData } = tasksContext



    // EFFECT
    // Detecta si hay una tarea seleccionada
    useEffect( () => {

        if( currentTask !== null ){
            updateTask(currentTask)
        } else {
            updateTask({
                name: ''
            })
        }

    }, [ currentTask ] )
    


    // Validando que haya un proyecto actuál
    if( !currentProject ) return null
    
    
    
    // Destructuring
    // del arreglo del proyecto actual
    const [ currentProjectItem ] = currentProject
    // de la tarea
    const { name } = task
    


    // FUNCIONES
    // Detectar cambios campo nombre
    const handleChangeName = e => {
        updateTask({
            ...task,
            [e.target.name]: e.target.value
        })
    }
    // Enviar nueva tarea
    const submitNewTask = e => {

        e.preventDefault()



        // Validar 
        if( name.trim() === '' ) {
            validateTask()
            return null
        }

        // Valida si crea una nueva tarea o edita
        if( currentTask !== null ){

            // Actualizar la tarea
            updateTaskData(task)
            
        } else {
            
            // Agregando otros valores al proyecto
            task.project = currentProjectItem._id // Id del proyecto para vincular la tarea a uno, como en el módelo el id del proyecto lo crea con _ se debe poner así
            task.id = uuidv4() // id de la tarea
            // task.taskState = false // por defecto el state (en este caso se refieren a si está completado o no) es false
    
            // Envia los datos al state superiot
            addTask(task)

        }

        // Obtener las tareas del proyecto de nuevo (para incluir la que fue creada)
        getTasksByProject(currentProjectItem._id)

        // Resetear el form
        updateTask({
            name: ''
        })


    }



    return (
        <div className="formulario">
            <form
                onSubmit={ e => submitNewTask(e) } >
                <div className="contenedor-input">
                    <input 
                        type="text"
                        className="input-text"
                        placeholder="Nombre Tarea..."
                        name="name"
                        value={ name }
                        onChange={ handleChangeName }
                        />
                </div>
                { 
                    errorTask 
                    ? <p className="mensaje error">El nombre de la tarea es obligatorio</p>
                    : null
                }
                <div className="contenedor-input">
                    <input
                        type="submit" 
                        className="btn btn-primario btn-submit btn-block"
                        value={ currentTask !== null ? 'Editar tarea' : 'Agregar tarea' }
                        />
                </div>
            </form>
        </div>
    )
}
 
export default FormTask