import React, { Fragment, useContext } from 'react'
// Importo los context de proyectos y de tareas
import projectContext from '../../context/projects/projectContext'
import taskContext from '../../context/tasks/taskContext'
// Importo componentes
import Task from './Task'
// Importo react transition group
import { CSSTransition, TransitionGroup } from 'react-transition-group'



const ListTasks = () => {

    // STATES



    // CONTEXT
    // Creando una variable con el contenido del context (lo extraemos con useContext)
    const projectsContext = useContext(projectContext)
    const tasksContext = useContext(taskContext)



    // Creando una variable con el contenido del context (lo extraemos con useContext)
    const { currentProject, deleteProject } = projectsContext
    const { tasksProject, deleteTasksByProject } = tasksContext

    

    // Validando que haya un proyecto actuál
    if( !currentProject ) return <h2>Selecciona un proyecto</h2>
    if( !tasksProject ) return null



    // Destructuring del arreglo del proyecto actual
    const [ currentProjectItem ] = currentProject



    // FUNCTIONES
    // Borrar proyecto con sus tareas
    const deleteAllProject = ( projectId ) => {
        deleteProject( projectId )
        // deleteTasksByProject( projectId )
    }



    return (
        <Fragment>
            <h2>Proyecto: { currentProjectItem.name }</h2>
            <ul className="listado-tareas">
                {
                    tasksProject.length < 1
                        ? 
                            (
                                <li className="tarea">
                                    <p>
                                        No hay tareas
                                    </p>
                                </li>
                            )
                        : 
                            <TransitionGroup>
                                {
                                    tasksProject.map( task => (
                                        <CSSTransition
                                            key={task._id}
                                            timeout={300}
                                            classNames="tarea" >
                                            <Task task={task} />
                                        </CSSTransition>
                                    ) )
                                }
                            </TransitionGroup>
                }
            </ul>
            <button
                type="button"
                className="btn btn-eliminar"
                onClick={ () => deleteAllProject( currentProjectItem._id ) } >
                Eliminar Proyecto &times;
            </button>
        </Fragment>
    )
}
 
export default ListTasks