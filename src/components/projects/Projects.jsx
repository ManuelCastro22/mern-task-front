import React, { useContext, useEffect } from 'react'
// Importo componentes
import Header from '../layout/Header'
import Sidebar from '../layout/Sidebar'
import FormTask from '../tasks/FormTask'
import ListTasks from '../tasks/ListTasks'
// Importamos la autenticación, necesaria para dar o no permisos a un usuario para acceder a determinados lugares
import authContext from '../../context/auth/authContext'



const Projects = () => {

    // CONTEXT
    // Creando una variable con el contenido del context (lo extraemos con useContext)
    const authsContext = useContext(authContext)



    // Obteniedo los datos a usar del context de autenticación
    const { userAuthenticated } = authsContext



    // EFFECT
    useEffect( () => {
        // Mantiene la sesión iniciada para el usuario que este en el token del local storage
        userAuthenticated()
    }, [ ] )



    return (
        <div className="contenedor-app">
            <Sidebar />
            <div className="seccion-principal">
                <Header />
                <main>
                    <FormTask />
                    <div className="contenedor-tareas">
                        <ListTasks />
                    </div>
                </main>
            </div>
        </div>        
    )
}
 
export default Projects