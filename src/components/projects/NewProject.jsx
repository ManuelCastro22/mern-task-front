import React, { Fragment, useState, useContext } from 'react' // importo fragmentes y hooks
// Importo los context de proyectos y tareas
import projectContext from '../../context/projects/projectContext'



const NewProject = () => {
    
    // CONTEXT
    // Creando una variable con el contenido del context (lo extraemos con useContext)
    const projectsContext = useContext(projectContext)



    // Creando una variable con el contenido del context (lo extraemos con useContext)
    const { hasForm, errorForm, showForm, addProject, showError } = projectsContext



    // STATES
    // Nuevo Proyecto
    const [ newProject, updateNewProject ] = useState({
        name: ''
    })



    // DESTRUCTURING
    // de Nuevo proyecto
    const { name } = newProject



    // FUNCTIONS
    // Input del nombre del proyecto
    const changeNameProject = e => {

        updateNewProject({
            ...newProject,
            [e.target.name]: e.target.value
        })

    }
    // Submit del form
    const submitNewProject = e => {

        e.preventDefault()

        // Validar
        if( name.trim() === '' ){
            showError()
            return null
        }

        // Enviar al state principal
        addProject(newProject)

        // Reinciar el form
        updateNewProject({
            name: ''
        })

    }


    return (
        <Fragment>
            <button
                type="button"
                className="btn btn-block btn-primario" 
                onClick={ showForm } >
                Nuevo Proyecto
            </button>
            {
                hasForm === true
                ?
                (
                    <form 
                        className="formulario-nuevo-proyecto" 
                        onSubmit={ e => submitNewProject(e) } >
                        <input 
                            type="text"
                            className="input-text"
                            placeholder="Nombre Proyecto"
                            value={ name }
                            name="name"
                            onChange={ changeNameProject } />
                        <input
                            type="submit"
                            className="btn btn-primario btn-block"
                            value="Agregar Proyecto" />
                    </form>
                )
                : null
            }

            {
                errorForm
                ? <p className="mensaje error">El nombre del proyecto es obligatorio</p>
                : null
            }
            
        </Fragment>
    )
}
 
export default NewProject