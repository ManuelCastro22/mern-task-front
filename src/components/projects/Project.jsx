import React, { useContext } from 'react' // Importo el hook useContext
// Importo los context de proyectos y tareas
import projectContext from '../../context/projects/projectContext'
import taskContext from '../../context/tasks/taskContext'



const Project = ( { project } ) => {

    // CONTEXT
    // Creando una variable con el contenido del context (lo extraemos con useContext)
    const projectsContext = useContext(projectContext)
    const tasksContexts = useContext(taskContext)



    // Creando una variable con el contenido del context (lo extraemos con useContext)
    const { currentProject, getCurrentProject } = projectsContext
    const { updateCurrentTask, getTasksByProject } = tasksContexts



    // FUNCIONES
    // Seleccionar un proyecto y traer sus datos
    const selectProject = idProject => {
        getCurrentProject(idProject)
        getTasksByProject(idProject)
        updateCurrentTask(null)
    }


    return (
        <li>
            <button 
                type="button"
                className="btn btn-blank"
                onClick={ () => selectProject(project._id) } >
                { project.name }
            </button>
        </li>
    )

}
 
export default Project