import React, { useContext, useEffect } from 'react' // importo hooks
// Importo componentes
import Project from './Project'
// importo los context de proyectos y alertas
import projectContext from '../../context/projects/projectContext'
import alertContext from '../../context/alerts/alertContext'
// Importo 'react-transition-group'
import { CSSTransition, TransitionGroup } from 'react-transition-group'



const ListProjects = () => {

    // CONTEXT
    // Creando una variable con el contenido del context (lo extraemos con useContext)
    const projectsContext = useContext(projectContext),
        alertsContext = useContext(alertContext)



    // Obteniedo los datos a usar del context de proyectos
    const { message, projects, getProjects } = projectsContext,
        { alert, showAlert } = alertsContext



    // EFFECT
    useEffect( () => {

        // Si hay error
        if(message) {
            showAlert(message.msg, message.category)
        }

        getProjects()
        
    }, [message] )



    // Validando valores
    if( projects.length === 0 ) return <p>No tienes proyectos, intenta creando uno</p>

    

    return (
        <ul className="listado-proyectos">

            { alert ?  <div className={`alerta ${alert.category}`}> {alert.msg} </div> : null  }

            {
                <TransitionGroup>
                    {
                        projects.map( project => (
                            <CSSTransition
                                key={project._id} // El primer hijo de la iteración será quien lleve key
                                timeout={300} >
                                <Project  project={project}/>
                            </CSSTransition>
                        ) )
                    }
                </TransitionGroup>
            }
        </ul>
    )
}
 
export default ListProjects