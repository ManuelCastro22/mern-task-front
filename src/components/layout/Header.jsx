import React, { useContext, useEffect } from 'react'
// Importo el context de alertas y auth
import authContext from '../../context/auth/authContext'



const Header = () => {

    // CONTEXT
    // Creando una variable con el contenido del context (lo extraemos con useContext)
    const authsContext = useContext(authContext)



    // Obteniendo los datos a usar del context de alertas
    const { user, userAuthenticated, logout } = authsContext



    // EFFECT
    useEffect( () => {
        // Mantiene la sesión iniciada para el usuario que este en el token del local storage
        userAuthenticated()
    }, [] )



    return (
        <header className="app-header">
            {/* Como tarda en traer los datos a la primera es null, pero luego como se está usando el hook useEffect leerá los datos y por ende el condicional se volverá a evaluar lo que mostrará el nombre */}
            {
                user ?
                    (
                        <p className="nombre-usuario">
                            Hola <span> {user.name} </span>
                        </p>
                    )
                    :   
                    null
                     
            }
            <nav className="nav-principal">
                <button
                    className="btn btn-blank cerrar-sesion"
                    onClick={ () => logout() }>
                    Cerrar sesión
                </button>
            </nav>
        </header>
    )
}
 
export default Header