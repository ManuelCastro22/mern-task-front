import React from 'react'
// Importo componentes
import NewProject from '../projects/NewProject'
import ListProjects from '../projects/ListProjects'



const Sidebar = () => {
    return (
        <aside>
            <h1> MERN <span>Task</span> </h1>

            <NewProject />

            <div className="proyectos">
                <h2>Tus proyectos </h2>
            </div>
            
            <ListProjects />

        </aside>
    )
}
 
export default Sidebar