import React, { useState, useContext, useEffect }  from 'react' // importo hooks
// Importamos React Router, lo instalamos con 'npm i react-router-dom'
import { Link } from 'react-router-dom'
// Importo el context de alertas y auth
import alertContext from '../../context/alerts/alertContext'
import authContext from '../../context/auth/authContext'



const Login = props => {

    // CONTEXT
    // Creando una variable con el contenido del context (lo extraemos con useContext)
    const alertsContext = useContext(alertContext)
    const authsContext = useContext(authContext)



    // Obteniendo los datos a usar del context de alertas
    const { alert, showAlert } = alertsContext
    const { message, authenticated, login } = authsContext



    // USE EFFECT
    // En caso de que el usuario se haya autenticado, iniciado sesión
    useEffect( () => {

        // Si está autenticado lo redirige al dashboard de proyectos
        if( authenticated ){
            props.history.push('/proyectos') // Url a redirigir
        }

        // Si existe un mensaje (es decir, ahí algún error)
        if( message ) {
            showAlert(message.msg, message.category)
        }

    }, [ message, authenticated, props.history ] ) // props.history, nos sirve para hacer redirecciones



    // STATES
    // State de Iniciar Sesión
    const [ user, updateUser ] = useState({
        email: '',
        password: ''
    })



    // DESTRUCTURING
    // de usuario
    const { email, password } = user



    // FUNCTIONS
    // Change para iniciar sesión
    const changeLogin = e => {
        updateUser({
            ...user,
            [e.target.name]: e.target.value
        })
    }

    // Submit del form
    const submitForm = e => {

        e.preventDefault()


        // Validación
        if( email.trim() === '' || password === '' ) {
            showAlert('Ingrese valores validos', 'alerta-error')
            return
        }
        
        // Pasarlo al action
        login({
            email,
            pass: password
        })

    }


    return (
        <div className="form-usuario">
            { alert ?  <div className={`alerta ${alert.category}`}> {alert.msg} </div> : null  }
            <div className="contenedor-form sombra-dark">
                <h1>Iniciar Sesión</h1>
                <form
                    onSubmit={submitForm} >
                    <div className="campo-form">
                        <label htmlFor="email"> Email </label>
                        <input 
                            type="email"
                            id="email"
                            name="email"
                            placeholder="Tu Email"
                            onChange={ changeLogin }
                            />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="password"> Contraseña </label>
                        <input 
                            type="password"
                            id="password"
                            name="password"
                            placeholder="Tu contraseña"
                            onChange={ changeLogin }
                            />
                    </div>
                    <div className="campo-form">
                        <input 
                            type="submit"
                            id="login"
                            name="login"
                            className="btn btn-primario btn-block"
                            value="Iniciar Sesión"
                            />
                    </div>
                </form>
                {/* Enlace que generamos con react-router-dom */}
                <Link to={'/nueva-cuenta'} className="enlace-cuenta">
                    Obtener Cuenta
                </Link>
            </div>
        </div>
    )
}
 
export default Login