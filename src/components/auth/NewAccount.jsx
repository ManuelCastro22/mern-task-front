import React, { useState, useContext, useEffect }  from 'react' // importo hooks
// Importamos React Router, lo instalamos con 'npm i react-router-dom'
import { Link } from 'react-router-dom'
// Importo el context de alertas
import alertContext from '../../context/alerts/alertContext'
import authContext from '../../context/auth/authContext'



const NewAccount = ( props ) => {  // Como usamos react-router-dom podemos hacer una redirección

    // CONTEXT
    // Creando una variable con el contenido del context (lo extraemos con useContext)
    const alertsContext = useContext(alertContext)
    const authsContext = useContext(authContext)



    // Obteniendo los datos a usar del context de alertas
    const { alert, showAlert } = alertsContext
    const { message, authenticated, registerUser } = authsContext



    // USE EFFECT
    // En caso de que el usuario se haya autenticado, registrado o sea un registro duplicado
    useEffect( () => {

        // Si está autenticado lo redirige al dashboard de proyectos
        if( authenticated ){
            props.history.push('/proyectos') // Url a redirigir
        }

        // Si existe un mensaje (es decir, ahí algún error)
        if( message ) {
            showAlert(message.msg, message.category)
        }

    }, [ message, authenticated, props.history ] ) // props.history, nos sirve para hacer redirecciones



    // STATES
    // State de Iniciar Sesión
    const [ user, updateUser ] = useState({
        name: '',
        email: '',
        password: '',
        passwordConfirm: ''
    })



    // DESTRUCTURING
    // de usuario
    const { name, email, password, passwordConfirm } = user



    // FUNCTIONS
    // Change para iniciar sesión
    const changeLogin = e => {
        updateUser({
            ...user,
            [e.target.name]: e.target.value
        })
    }

    // Submit del form
    const submitForm = e => {

        e.preventDefault()


        // Validación
        if( name.trim() === '' ||
            email.trim() === '' ||
            password.trim() === '' ||
            passwordConfirm.trim() === '' ) {
            showAlert('Ingrese valores validos', 'alerta-error')
            return
        }
        
        
        // Pass iguales y de mínimo de carácteres
        if( password.length < 6 ){
            showAlert('La contraseña debe ser de mínimo 6 carácteres', 'alerta-error')
            return
        }
        
        if( password !== passwordConfirm ){
            showAlert('Las contraseñas no coinciden', 'alerta-error')
            return
        }


        // Pasarlo al action
        registerUser({
            name,
            email,
            pass: password
        })

    }


    return (
        <div className="form-usuario">
            { alert ?  <div className={`alerta ${alert.category}`}> {alert.msg} </div> : null  }
            <div className="contenedor-form sombra-dark">
                <h1>Obtener una cuenta</h1>
                <form
                    onSubmit={submitForm} >
                    <div className="campo-form">
                        <label htmlFor="name"> Nombre </label>
                        <input 
                            type="text"
                            id="name"
                            name="name"
                            placeholder="Tu nombre"
                            onChange={ changeLogin }
                            />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="email"> Email </label>
                        <input 
                            type="email"
                            id="email"
                            name="email"
                            placeholder="Tu email"
                            onChange={ changeLogin }
                            />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="password"> Contraseña </label>
                        <input 
                            type="password"
                            id="password"
                            name="password"
                            placeholder="Tu contraseña"
                            onChange={ changeLogin }
                            />
                    </div>
                    <div className="campo-form">
                        <label htmlFor="passwordConfirm"> Repetir Contraseña </label>
                        <input 
                            type="password"
                            id="passwordConfirm"
                            name="passwordConfirm"
                            placeholder="Repetir Contraseña"
                            onChange={ changeLogin }
                            />
                    </div>
                    <div className="campo-form">
                        <input 
                            type="submit"
                            id="signin"
                            name="signin"
                            className="btn btn-primario btn-block"
                            value="Registrarme"
                            />
                    </div>
                </form>
                {/* Enlace que generamos con react-router-dom */}
                <Link to={'/'} className="enlace-cuenta">
                    Volver a Iniciar Sesión
                </Link>
            </div>
        </div>
    )
}
 
export default NewAccount