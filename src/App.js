import React from 'react'
// Importamos React Router, lo instalamos con 'npm i react-router-dom'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'
// importo componentes
import Login from './components/auth/Login'
import NewAccount from './components/auth/NewAccount'
import Projects from './components/projects/Projects'
// importo los states
import ProjectState from './context/projects/ProjectState'
import TaskState from './context/tasks/TaskState'
import AlertState from './context/alerts/AlertState'
import AuthState from './context/auth/AuthState'
// importo la función para agregar al header el token
import authToken from './config/token'
// importo el higher order component para usar rutas privadas
import PrivateRoutes from './components/routes/PrivateRoutes'



// Validamos si existe un token, de haberlo lo agregramos al header de la petición
const token = localStorage.getItem('token')
if( token ){
    authToken(token)
}



function App() {

// Usando variables de entorno para trabajar en local, creamos un archivo .env.development.local en donde creamos las variables de entorno en local, esas variables de entorno siempre deben comenzar con REACT_APP...
console.log(process.env.REACT_APP_BACKEND_URL)



// Todo lo que este afuera de switch se verá en todas la páginas, mientras que lo que se encuentre adentro será cada una de las paáginas, Route hace referencia a la ruta exacta y el componente que se llamará, Link hace referencia a una redirección
return (

    <ProjectState>
        <TaskState>
            <AlertState>
                <AuthState>
                    <Router>
                        <Link to={'/'} className="ml5 mr5">Login</Link>
                        <Link to={'/nueva-cuenta'} className="ml5 mr5">New Accoung</Link>
                        <Link to={'/proyectos'} className="ml5 mr5">Projects</Link>
                        <Switch>
                            <Route exact path="/" component={Login} />
                            <Route exact path="/nueva-cuenta" component={NewAccount} />
                            <PrivateRoutes exact path="/proyectos" component={Projects} /> 
                        </Switch>
                    </Router>
                </AuthState>
            </AlertState>
        </TaskState>
    </ProjectState>

    )
}

export default App